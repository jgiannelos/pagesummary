# Summary extension
## Description
Prototyping work to port the nodejs implementation of `/page/summary` from PCS services to MW.

## Quick start
1. Install PHP and composer
2. `composer install`

Once set up, running `composer test` will run automated code checks.
Once the extension is installed in a MW instance the endpoint is registered in this path:
* `/w/rest.php/v1/summary/<title>`.