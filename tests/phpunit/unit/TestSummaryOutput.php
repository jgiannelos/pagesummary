<?php
use MediaWiki\Extension\PageSummary\SummaryOutput;
use PHPUnit\Framework\TestCase;

final class TestSummaryOutput extends TestCase {
	public function testSummaryEndpointStatusCode(): void {
		$title = "Main_Page";
		$output = new SummaryOutput( $title );
		$this->assertEquals( $output->title, $title );
		$this->assertEquals( $output->generate(), [ "description" => "Description for page Main_Page" ] );
	}
}
