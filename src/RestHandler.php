<?php
namespace MediaWiki\Extension\PageSummary;

use MediaWiki\Rest\Handler;
use MediaWiki\Rest\Response;
use MediaWiki\Rest\SimpleHandler;
use Wikimedia\ParamValidator\ParamValidator;

/**
 * Handler for displaying or rendering a page summary:
 * - /v1/page/summary/{format}
 */
class RestHandler extends SimpleHandler {
	public function needsWriteAccess() {
		return false;
	}

	/**
	 * Handle request for page summary
	 * @param string $title
	 * @return Response
	 */
	public function run( $title ) {
		// TODO: Implement summary response body
		$output = new SummaryOutput( $title );
		$response = $this->getResponseFactory()->createJson( $output->generate() );
		return $response;
	}

	/**
	 * Extract parameters from URL route
	 * @return array[]
	 */
	public function getParamSettings() {
		return [
			'title' => [
				Handler::PARAM_SOURCE => 'path',
				ParamValidator::PARAM_TYPE => 'string',
				ParamValidator::PARAM_REQUIRED => true,
			]
		];
	}
}
