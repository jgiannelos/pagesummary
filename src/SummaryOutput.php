<?php
namespace MediaWiki\Extension\PageSummary;

/**
 * Page summary output
 */
class SummaryOutput {
	/** @var string */
	public $title;

	/**
	 * @param string $title The title of the page to summarize
	 */
	public function __construct( $title ) {
		$this->title = $title;
	}

	public function generate() {
		return [
			"description" => "Description for page $this->title"
		];
	}
}
